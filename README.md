# Instant Enrollment Portal


**Authors**: Kelving Heroo,	Maurice Jules Mulisa

**Reference**: (Project Number)

**Date**: 05/11/2018

**Version**: 1.0

## Project Introduction

This document describes in detail how webservices requests are sent from the client’s web application which has a user friendly interface to minimize errors to the gateway.  The gateway will respond back with the required response. All the requests sent from the web application will be authenticated (using token) first then processed. The gateway acts as an intermediate between the client application and ICPS web services.

## Project Creation Verification and Approval 
| Creation | Verification | Approval |
| :-----------: | :------: | :------------: | 
| Maurice Jules Mulisa     | Kelving Heroo  | Arnaud BAZERQUE BACHA      |

## Document History 

| Version       | Author                | Date                      | Purpose               | 
| :-----------: | :--------------------:| :------------------------:| :-------------------: |
| 1.0           | Maurice Jules Maurice | Monday, November 5, 2018  | Initial Documentation |
| 1.1           | Kelhe                 | Monday, November 12, 2018 | Second Documentation  |
|               |                       |                           |                       |
|               |                       |                           |                       |

## System Archtecture

